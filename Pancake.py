#  File: Pancake.py

#  Description: Sorts a list by flipping the order of higher index elements.

#  Student's Name: Christopher Calizzi

#  Student's UT EID: csc3322

#  Course Name: CS 313E

#  Unique Number: 50295

#  Date Created:2-17-20

#  Date Last Modified:2-17-20

#  Input: pancakes is a list of positive integers
#  Output: a list of the pancake stack each time you
#          have done a flip with the spatula
#          this is a list of lists
#          the last item in this list is the sorted stack
def sort_pancakes ( pancakes ):
  every_flip = []
  sorted_index = len(pancakes)
  while sorted_index !=0:
      index = final_index_max(pancakes, sorted_index)
      if index == sorted_index-1:
          sorted_index -=1
      elif pancakes.index(max(pancakes[0:sorted_index]))==0:
          pancakes = flip(pancakes, sorted_index)
          every_flip.append(pancakes)
          sorted_index -= 1
      else:
          pancakes = flip(pancakes,index+1)
          every_flip.append(pancakes)
          pancakes = flip(pancakes,sorted_index)
          every_flip.append(pancakes)
          sorted_index -=1
  return every_flip    # return a list of flipped pancake stacks
def flip(pancakes,index):
    flipped = []
    for i in range(index-1,-1,-1):
        flipped.append(pancakes[i])
    flipped.extend(pancakes[index:len(pancakes)])
    return flipped
def check_done(pancakes):
    i = 0
    done = True
    while i<len(pancakes)-1 and done:
        done = pancakes[i]<pancakes[i+1]
    return done
def final_index_max(pancakes,sorted_index):
    largest = max(pancakes[0:sorted_index])
    for index in range(sorted_index-1,-1,-1):
        if pancakes[index] == largest:
            return index
def main():
  # open the file pancakes.txt for reading
  in_file = open ("./pancakes.txt", "r")

  line = in_file.readline()
  line = line.strip()
  line = line.split()
  print (line)
  pancakes = []
  for item in line:
    pancakes.append (int(item))

  # print content of list before flipping
  print ("Initial Order of Pancakes = ", pancakes)

  # call the function to sort the pancakes
  every_flip = sort_pancakes ( pancakes )

  # print the contents of the pancake stack after
  # every flip
  for i in range (len(every_flip)):
    print (every_flip[i])

  # print content of list after all the flipping
  print ("Final Order of Pancakes = ", every_flip[-1])

if __name__ == "__main__":
  main()